// Create user schema
var userSchema = mongoose.Schema({
	username: String,
	password: String,
	fullName: String
});

var Person = mongoose.model('users', userSchema);

var John = new Person({
	username: 'johndoe',
	password: 'john_wants_to_login',
	fullName: ' John Doe'
});

John.save(function(err) {
	if (!err) {
		console.log('User saved successfully!');
	}
});