'use strict';

var express = require('express'),
	path = require('path'),
	cookieParser = require('cookie-parser'),
	session = require('express-session'),
	config = require('./config/config.js'),
	ConnectMongo = require('connect-mongo')(session),
	mongoose = require('mongoose').connect(config.dbUrl),
	passport = require('passport'),
	FacebookStrategy = require('passport-facebook').Strategy,
	rooms = [],
	app = express();

// Tell express where view files are located from root
// Note: __dirname refers to the root folder
app.set('views', path.join(__dirname, 'views'))

// To use hogan template engine and make the engine use html
app.engine('html', require('hogan-express'));

// Is used to tell express not to look for a default rendering
// engine based on file extensions but to use html files
app.set('view engine', 'html');

// Where to find static files
app.use(express.static(path.join(__dirname, 'public')));

app.use(cookieParser());

var env = process.env.NODE_ENV || 'development';
if (env === 'development') {
	// dev specific settings
	app.use(session({
		secret: config.sessionSecret,
		saveUninitialized: true,
		resave: true
	}));
} else {
	// production specific settings
	app.use(session({
		secret: config.sessionSecret,
		store: new ConnectMongo({
			// url: config.dbUrl, Disabled, because mongoose has already created a connnection, 
			// doing this avoids duplication of connections to the mongolab db
			mongoose_connection: mongoose.connections[0],
			stringify: true
		}),
		saveUninitialized: true,
		resave: true
	}));
}

// To initialize passport
app.use(passport.initialize());
app.use(passport.session());

require('./routes/routes.js')(express, app, passport, config);
require('./auth/passportAuth.js')(passport, FacebookStrategy, config, mongoose);

app.set('port', process.env.PORT || 3000);

var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var socket = require('./socket/socket.js')(io, rooms);

server.listen(app.get('port'), function() {
	console.log('Mode: ' + env);
	console.log('ChatCat working on port: ' + app.get('port'));
});
// app.listen(3000, function() {
// 	console.log('ChatCat working on port 3000');
// 	console.log('Mode: ' + env);
// });