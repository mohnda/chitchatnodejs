'use strict';

module.exports = function(express, app, passport, config) {
	var router = express.Router();
	router.get('/', function(req, resp, next) {
		//resp.send('<h1>Hello Mo!</h1>');
		resp.render('index', {title: 'Welcome to ChatCAT'});
	});

	function securePages(req, resp, next) {
		if (req.isAuthenticated()) {
			next();
		} else {
			resp.redirect('/');
		}
	}

	function findNameBy(room_id) {
		var index = 0;
		while(index < rooms.length) {
			if (rooms[index].room_number === room_id) {
				return rooms[index].room_name;
			} else{
				index++;
			}
		}
	}

	router.get('/auth/facebook', passport.authenticate('facebook'));
	router.get('/auth/facebook/callback', passport.authenticate('facebook', {
		successRedirect: '/chatrooms',
		failureRedirect: '/'
	}));

	router.get('/chatrooms', securePages, function(req, resp, next) {
		resp.render('chatrooms', {
			title: 'ChatCAT - Chatrooms',
			user: req.user,
			config: config
		});
	});

	router.get('/rooms/:id', function(req, resp, next) {
		var room_name = findNameBy(req.params.id);
		resp.render('room', {
			user: req.user,
			room_number: req.params.id,
			room_name: room_name,
			config: config
		});
	});

	router.get('/logout', function(req, resp, next) {
		req.logout();
		resp.redirect('/');
	});

	// router.get('/setcolor', function(req, resp, next) {
	// 	req.session.favColor = 'Blue';
	// 	resp.send('Favourite color set');
	// });

	// router.get('/getcolor', function(req, resp, next) {
	// 	resp.send('The favourite color is ' + (req.session.favColor === undefined ? 'No color set' : req.session.favColor));
	// });

	app.use('/', router);
}
